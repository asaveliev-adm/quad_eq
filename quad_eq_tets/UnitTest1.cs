﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace quad_eq_tets
{
    [TestClass]
    public class discr_tests
    {
        
        [TestMethod]
        public void discr_tests_is_negative()
        {
            double a = 5;
            double b = 3;
            double c = 7;
            double d = quad_eq.Program.discr(a, b, c);
            Assert.AreEqual(-131, d);
        }

        [TestMethod]
        public void discr_tests_is_positive()
        {
            double a = 1;
            double b = -8;
            double c = 12;
            double d = quad_eq.Program.discr(a, b, c);
            Assert.AreEqual(16, d);
        }

        [TestMethod]
        public void discr_tests_is_null()
        {
            double a = 1;
            double b = -6;
            double c = 9;
            double d = quad_eq.Program.discr(a, b, c);
            Assert.AreEqual(0, d);

        }
    }

    [TestClass]
    public class quad_eq_tests
    {

        [TestMethod]
        public void quad_eq_single_ans()
        {
            double a = 1;
            double b = -6;
            double c = 9;
            double x1 = 0, x2 = 0;
            bool t = quad_eq.Program.eq_det(a, b, c, ref x1, ref x2);
            Assert.IsTrue(t);
            Assert.AreEqual(3, x1);
            Assert.AreEqual(x1, x2);
        }

        [TestMethod]
        public void quad_eq_two_ans()
        {
            double a = 1;
            double b = -8;
            double c = 12;
            double x1 = 0, x2 = 0;
            bool t = quad_eq.Program.eq_det(a, b, c, ref x1, ref x2);
            Assert.IsTrue(t);
            Assert.AreEqual(6, x1);
            Assert.AreEqual(2, x2);
        }

        [TestMethod]
        public void quad_eq_wo_ans()
        {
            double a = 5;
            double b = 3;
            double c = 7;
            double x1 = 0, x2 = 0;
            bool t = quad_eq.Program.eq_det(a, b, c, ref x1, ref x2);
            Assert.IsFalse(t);
            Assert.AreEqual(0, x1);
            Assert.AreEqual(0, x2);
        }

        [TestMethod]
        [ExpectedException(typeof(quad_eq.Program.UserNotQudraticEquationException))]
        public void quad_eq_a_is_null()
        {
            double a = 0;
            double b = 0;
            double c = 0;
            double x1 = 0, x2 = 0;
            bool t = quad_eq.Program.eq_det(a, b, c, ref x1, ref x2);
            Assert.IsFalse(t);
            Assert.AreEqual(0, x1);
            Assert.AreEqual(0, x2);
        }

    }
}
