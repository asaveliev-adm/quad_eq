﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quad_eq
{
    public class Program
    {
        static void Main(string[] args)
        {
            double a=0, b=0, c=0, x1=0, x2=0; //неинициализированные переменные c# не позволяет использовать
            Console.WriteLine("please enter thr factors (each in newline)");
            try
            {
                a = Convert.ToDouble(Console.ReadLine());
                b = Convert.ToDouble(Console.ReadLine());
                c = Convert.ToDouble(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("NAN");
                Console.ReadLine();
                System.Environment.Exit(1);
            }
            bool t = eq_det(a, b, c, ref x1, ref x2);
            if (t)
            {
                Console.WriteLine("x1 = {0}",  x1);
                Console.WriteLine("x2 = {0}",  x2);
                Console.ReadLine();
            }
            
        }
        public static double discr(double a, double b, double c)
        {
            double d = Math.Pow(b, 2) - 4 * a * c;
            return d;
        }
        public static bool eq_det(double a, double b, double c, ref double x1, ref double x2)
        {
            if (a<=0)
            {
                throw new UserNotQudraticEquationException("first argument is null");
            }
            double d = discr(a, b, c);
            if (d>=0)
            {
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
                x2 = (-b - Math.Sqrt(d)) / (2 * a);
                return true;
            }
            else
            {
                return false;
            }
        }


        [Serializable]
        public class UserNotQudraticEquationException : ApplicationException
        {
            public UserNotQudraticEquationException() { }
            public UserNotQudraticEquationException(string message) : base(message) { }
            public UserNotQudraticEquationException(string message, Exception inner) : base(message, inner) { }
            protected UserNotQudraticEquationException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context) : base(info, context)
            { }
        }

    }
}
